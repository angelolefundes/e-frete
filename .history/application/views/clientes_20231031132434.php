  <div class="container py-4">
        <div class="py-5 text-center">
            <img class="d-block mx-auto mb-4" src="/docs/5.3/assets/brand/bootstrap-logo.svg" alt="" width="72" height="57">
            <h2>Cadastro de usuário</h2>
            <p class="lead">Após concluir seu cadastro, você poderá imediatamente começar a usar nossa plataforma.</p>
        </div>  
        <div id="retorno-sucesso" class="row g-5" style="display: none">
          <span>Prezado, <span id="nome-usuario"></span><br>Cadastro realizado com sucesso!</span>
          <p><a href="<?=base_url()?>">Clique para continuar</a></p>
        </div>
        <div id="formulario" class="row g-5">
        <div class="col-md-7 col-lg-12">
            <div class="row">
            <div class="col-md-12">
                <div class="widget-wrap material-table-widget">

                    <div class="widget-container margin-top-0">
                        <div class="widget-content">
                            <div class="data-action-bar">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="widget-header">
                                            <h3>LISTAGEM DE CLIENTES</h3>
                                            <p>
                                                Listagem geral de todos os clientes cadastrados no sistema
                                            </p>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <table id="datatable" class="table table-striped table-bordered foo-data-table-filterable default footable-loaded footable" style="white-space: nowrap;">
                                <thead>
                                <tr>
                                    <th class="font-weight-bold" style="font-weight: bold !important;">NOME</th>
                                    <th class="font-weight-bold" style="font-weight: bold !important;">EMAIL</th>
                                    <th class="font-weight-bold" style="font-weight: bold !important;">CPF</th>
                                    <th class="font-weight-bold text-center" style="text-align:center; font-weight: bold !important;">AÇÃO</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        </div>