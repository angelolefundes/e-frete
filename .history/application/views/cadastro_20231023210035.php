  <div class="container py-4">
        <div class="py-5 text-center">
            <img class="d-block mx-auto mb-4" src="/docs/5.3/assets/brand/bootstrap-logo.svg" alt="" width="72" height="57">
            <h2>Cadastro de usuário</h2>
            <p class="lead">Após concluir seu cadastro, você poderá imediatamente começar a usar nossa plataforma.</p>
        </div>  
        <div class="row g-5">
        <div class="col-md-7 col-lg-8">
            <h4 class="mb-3">Dados pessoais</h4>
            <form id="j-forms-cadastro" class="needs-validation" novalidate="">
            <div class="row g-3">
                <div class="col-sm-6">
                <label for="firstName" class="form-label">Primeiro nome <span class="text-body-secondary">(Obrigatório)</span></label>
                <input type="text" class="form-control" id="firstName" placeholder="" value="" required="">
                <div class="invalid-feedback">
                    Valid first name is required.
                </div>
                </div>

                <div class="col-sm-6">
                <label for="lastName" class="form-label">Segundo nome <span class="text-body-secondary">(Obrigatório)</span></label>
                <input type="text" class="form-control" id="lastName" placeholder="" value="" required="">
                <div class="invalid-feedback">
                    Valid last name is required.
                </div>
                </div>

                <div class="col-12">
                <label for="username" class="form-label">CPF <span class="text-body-secondary">(Obrigatório)</span></label>
                <div class="input-group has-validation">
                    <span class="input-group-text">@</span>
                    <input type="text" class="form-control" id="username" placeholder="" required="">
                <div class="invalid-feedback">
                    Your username is required.
                    </div>
                </div>
                </div>

                <div class="col-12">
                <label for="email" class="form-label">Email <span class="text-body-secondary">(Obrigatório)</span></label>
                <input type="email" class="form-control" id="email" placeholder="">
                <div class="invalid-feedback">
                    Please enter a valid email address for shipping updates.
                </div>
                </div>

                <div class="col-12">
                <label for="address" class="form-label">Logradouro</label>
                <input type="text" class="form-control" id="address" placeholder="" required="">
                <div class="invalid-feedback">
                    Please enter your shipping address.
                </div>
                </div>

                <div class="col-12">
                <label for="address2" class="form-label">Complemento <span class="text-body-secondary">(Optional)</span></label>
                <input type="text" class="form-control" id="address2" placeholder="">
                </div>

                <div class="col-md-5">
                <label for="country" class="form-label">Estado</label>
                <select class="form-select" name="uf" id="uf" required="">
                    <option disabled selected>Escolha uma opção</option>
                </select>
                <div class="invalid-feedback">
                    Please select a valid country.
                </div>
                </div>

                <div class="col-md-4">
                <label for="state" class="form-label">Cidade</label>
                <select class="form-select" name="cidades" id="cidades" required="">
                    <option disabled selected>Escolha uma opção</option>
                </select>
                <div class="invalid-feedback">
                    Please provide a valid state.
                </div>
                </div>

                <div class="col-md-3">
                <label for="zip" class="form-label">Cep</label>
                <input type="text" class="form-control" id="zip" placeholder="" required="">
                <div class="invalid-feedback">
                    Zip code required.
                </div>
                </div>
            </div>

            <hr class="my-4">

            <div class="form-check">
                <input type="checkbox" class="form-check-input" id="same-address">
                <label class="form-check-label" for="same-address">Li e aceito os termos de <a href="#">política de privacidade</a></label>
            </div>

            <hr class="my-4">
            <button id="btn-gravar-cadastro" class="w-100 btn btn-primary btn-lg">Salvar dados</button>
            </form>
        </div>
        </div>