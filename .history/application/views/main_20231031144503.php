<!-- Begin page content -->
<main role="main" class="container">
  <div class="bg-body-tertiary p-5 rounded">
    <h1>Dashboard</h1>
    <p class="lead">De modo simples, a nossa plataforma, é um conjunto de elementos tecnológicos disponíveis na internet. É um local virtual onde são disponibilizadas ferramentas que permite agendar de maneira simples a consulta médica solicitada.</p>
    <a class="btn btn-lg btn-primary" href="<?=base_url()?>pacientes/cadastrar" role="button">Novo Paciente »</a>
  </div>
</main>