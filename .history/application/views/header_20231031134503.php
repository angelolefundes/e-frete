<!doctype html>
<html lang="en" data-bs-theme="auto">
  <head><script src="<?=base_url()?>assets/js/color-modes.js"></script>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.118.2">
    <title>e-Frete - Sistema de fretamento eletrônico</title>

    <link href="<?=base_url()?>assets/css/bootstrap.css" rel="stylesheet" crossorigin="anonymous">
    <link href="<?=base_url()?>assets/css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
    <!--Alerts-->
    <link href="<?=base_url();?>assets/css/toastr.min.css" rel="stylesheet" type="text/css"/>    
	
    <!-- Datatables -->
    <link href="<?=base_url()?>assets/plugins/DataTables/datatables.min.css" rel="stylesheet">
    <!-- Favicons -->
    <link rel="apple-touch-icon" href="/docs/5.3/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
    <link rel="icon" href="/docs/5.3/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="/docs/5.3/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
    <link rel="manifest" href="/docs/5.3/assets/img/favicons/manifest.json">
    <link rel="mask-icon" href="/docs/5.3/assets/img/favicons/safari-pinned-tab.svg" color="#712cf9">
    <link rel="icon" href="/docs/5.3/assets/img/favicons/favicon.ico">
    <meta name="theme-color" content="#712cf9">

    <link href="<?=base_url()?>assets/css/app.css" rel="stylesheet" crossorigin="anonymous">

  </head>
    <script>
      var base_url = "<?php echo base_url();?>";
    </script>
  <body>