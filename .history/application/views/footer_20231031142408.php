<footer class="footer">
  <div class="container">
    <span class="text-muted">Sistema para agenda de consultas populares</span>
  </div>
</footer>

<script src='https://code.jquery.com/jquery-3.7.1.min.js'></script>

<!-- DataTable -->
<script src="<?=base_url()?>assets/plugins/DataTables/datatables.min.js"></script>

<!-- Jquery Mask -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js" integrity="sha512-pHVGpX7F/27yZ0ISY+VVjyULApbDlD0/X0rgGbTqCE7WFW5MezNTWG/dnhtbBuICzsd0WQPgpE4REBLv+UqChw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<!--Alerts-->
<script src="<?=base_url();?>assets/js/toastr.min.js"></script>

<script src='<?=base_url()?>assets/js/bootstrap.js'></script>
<script src='<?=base_url()?>assets/js/bootstrap.min.js'></script>
<script src='<?=base_url()?>assets/js/app.js'></script>
  </body>
</html>