  <div class="container py-4">
  <header>
      <!-- Fixed navbar -->
      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="#">Fixed navbar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Link</a>
            </li>
            <li class="nav-item">
              <a class="nav-link disabled" href="#">Disabled</a>
            </li>
          </ul>
          <form class="form-inline mt-2 mt-md-0">
            <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
          </form>
        </div>
      </nav>
    </header>

    <div class="p-5 mb-4 bg-body-tertiary rounded-3">
      <div class="container-fluid py-5">
        <h1 class="display-5 fw-bold">Junte-se a nós!</h1>
        <p class="col-md-8 fs-4">De modo simples, a nossa plataforma, é um conjunto de elementos tecnológicos disponíveis na internet. É um local virtual onde são disponibilizadas ferramentas que permite aos seus usuários a o agendamento de fretes com o melhor custo-benefício do mercado.</p>
        <a href="<?=base_url()?>home/cadastro" class="btn btn-primary btn-lg" type="button">Criar uma conta</a>
      </div>
    </div>

    <div class="row align-items-md-stretch">
      <div class="col-md-6">
        <div class="h-100 p-5 text-bg-dark rounded-3">
          <h2>Mapa de atuação</h2>
          <p>Estamos presentes em quase todo território brasileiro. Nossos parceiros possuem a mais diversidade frota de caminhões e caçambas, temos um portfólio completo para atender as suas necessidades a qualquer dia e hora. Navegue no mapa e conheça as regiões que atendemos.</p>
          <button class="btn btn-outline-light" type="button">Visualizar mapa</button>
        </div>
      </div>
      <div class="col-md-6">
        <div class="h-100 p-5 bg-body-tertiary border rounded-3">
          <h2>Fale conosco</h2>
          <p>Aqui entregamos o melhor suporte ao cliente. Fale com um dos nossos especialistas e tire suas dúvidas o quanto antes. Estamos focados em resolver seu problema!</p>
          <button class="btn btn-outline-secondary" type="button">Falar com o especialista</button>
        </div>
      </div>
    </div>
    
