<!-- Begin page content -->
<main role="main" class="container">
  <h1 class="mt-5">Sticky footer with fixed navbar</h1>
  <p class="lead">Pin a fixed-height footer to the bottom of the viewport in desktop browsers with this custom HTML and CSS. A fixed navbar has been added with <code>padding-top: 60px;</code> on the <code>body &gt; .container</code>.</p>
  <p>Back to <a href="../sticky-footer/">the default sticky footer</a> minus the navbar.</p>

  <div class="bg-body-tertiary p-5 rounded">
    <h1>Navbar example</h1>
    <p class="lead">This example is a quick exercise to illustrate how fixed to top navbar works. As you scroll, it will remain fixed to the top of your browser’s viewport.</p>
    <a class="btn btn-lg btn-primary" href="/docs/5.3/components/navbar/" role="button">View navbar docs »</a>
  </div>
</main>