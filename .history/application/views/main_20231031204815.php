<!-- Begin page content -->
<main role="main" class="container">
  <h1 class="mt-5">Dashboard</h1>
  <p class="lead"></p>
  <p></p>

<div class="bg-body-tertiary p-5 rounded">
  <h1>Agendar Consulta</h1>
  <p class="lead">De modo simples, a nossa plataforma, é um conjunto de elementos tecnológicos disponíveis na internet. É um local virtual onde são disponibilizadas ferramentas que permite o acesso fácil e rápido para agendar consultas médicas solicitadas por meio do programa Saúde Popular na Faculdade Anhanguera.</p>
  <a class="btn btn-lg btn-primary" href="<?=base_url()?>consulta/cadastrar" role="button">Agendar Consulta »</a>
</div>

<div class="bg-body-tertiary p-5 rounded">
  <h1>Ligar/Desligar lâmpada</h1>
  <a id="btn-automacao" class="btn btn-lg btn-primary ligar" role="button">Ligar</a>
</div>
</main>