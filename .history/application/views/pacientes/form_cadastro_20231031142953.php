<div class="container py-4">
        <div id="formulario" class="row g-5">
        <div class="col-md-12 col-lg-12">
                <div class="widget-wrap material-table-widget">

                    <div class="widget-container margin-top-0">
                        <div class="widget-content">
                            <div class="data-action-bar">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="widget-header">
                                            <h3>CADASTRO DE PACIENTE</h3>
                                            <p>
                                                Informe os dados conforme solicitado observando para as informações obrigatórias
                                            </p>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <div id="formulario" class="row g-5">
            <div class="col-md-7 col-lg-8">
                <h4 class="mb-3">Dados pessoais</h4>
                <form id="j-forms-cadastro" class="needs-validation" novalidate="">
                <div class="row g-3">
                    <div class="col-sm-6">
                    <label for="firstName" class="form-label">Primeiro nome <span class="text-body-secondary">(Obrigatório)</span></label>
                    <input type="text" class="form-control" name="nome" id="nome" placeholder="" required="">
                    <div class="invalid-feedback">
                        Este campo é obrigatório
                    </div>
                    </div>

                    <div class="col-sm-6">
                    <label for="lastName" class="form-label">Segundo nome <span class="text-body-secondary">(Obrigatório)</span></label>
                    <input type="text" class="form-control" name="sobrenome" id="sobrenome" placeholder="" value="" required="">
                    <div class="invalid-feedback">
                        Este campo é obrigatório
                    </div>
                    </div>

                    <div class="col-12">
                    <label for="username" class="form-label">CPF <span class="text-body-secondary">(Obrigatório)</span></label>
                    <div class="input-group has-validation">
                        <input type="text" class="form-control" name="cpf" id="cpf" placeholder="000.000.000-00" required="">
                    <div class="invalid-feedback">
                        Your username is required.
                        </div>
                    </div>
                    </div>

                    <div class="col-12">
                    <label for="email" class="form-label">Email <span class="text-body-secondary">(Obrigatório)</span></label>
                    <input type="email" class="form-control" name="email" id="email" placeholder="">
                    <div class="invalid-feedback">
                        Please enter a valid email address for shipping updates.
                    </div>
                    </div>

                    <div class="col-12">
                    <label for="address" class="form-label">Logradouro</label>
                    <input type="text" class="form-control" name="logradouro" id="logradouro" placeholder="" required="">
                    <div class="invalid-feedback">
                        Please enter your shipping address.
                    </div>
                    </div>

                    <div class="col-12">
                    <label for="address2" class="form-label">Complemento <span class="text-body-secondary">(Optional)</span></label>
                    <input type="text" class="form-control" name="complemento" id="complemento" placeholder="">
                    </div>

                    <div class="col-md-5">
                    <label for="country" class="form-label">Estado</label>
                    <select class="form-select" name="estado_id" id="uf" required="">
                        <option disabled selected>Escolha uma opção</option>
                    </select>
                    <div class="invalid-feedback">
                        Please select a valid country.
                    </div>
                    </div>

                    <div class="col-md-4">
                    <label for="state" class="form-label">Cidade</label>
                    <select class="form-select" name="cidade_id" id="cidades" required="">
                        <option disabled selected>Escolha uma opção</option>
                    </select>
                    <div class="invalid-feedback">
                        Please provide a valid state.
                    </div>
                    </div>

                    <div class="col-md-3">
                    <label for="zip" class="form-label">Cep</label>
                    <input type="text" class="form-control" name="cep" id="cep" placeholder="" required="">
                    <div class="invalid-feedback">
                        Zip code required.
                    </div>
                    </div>
                </div>

                <hr class="my-4">
                <button id="btn-gravar-cadastro" class="w-100 btn btn-primary btn-lg">Salvar dados</button>
                </form>
            </div>
            </div>
                        </div>
                    </div>
                </div>
            
        </div>
        </div>