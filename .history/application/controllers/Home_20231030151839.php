<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->model('estados_model');
		$this->load->library('form_validation');
	}

	public function index()
	{
		$this->load->view('header');
		$this->load->view('main');
		$this->load->view('footer');
	}

	public function cadastro()
	{
		$this->load->view('header');
		$this->load->view('cadastro');
		$this->load->view('footer');
	}

	public function suporte()
	{
		$this->load->view('header');
		$this->load->view('suporte');
		$this->load->view('footer');
	}

	public function estados()
	{
		// Recupere os estados do banco de dados
		$estados = $this->estados_model->getAll(); 
	
		// Retorne os estados em formato JSON.
		echo json_encode(array("estados" => $estados));
	}

	public function getCidadesByUf()
	{
		// Obtém o estado_id da solicitação AJAX (enviado a partir do JavaScript)
		$estado_id = $this->input->post('estado_id');

		// Chame a função do modelo para obter as cidades do estado
		$cidades = $this->estados_model->getCidadesByUf($estado_id);

		// Retorne as cidades em formato JSON
		echo json_encode(array("cidades" => $cidades));
	}

	public function gravarCadastro()
    {
        try{
			$dados = (object)$this->input->post(null, true);
			$dados->cpf = Funcoes_diversas::formatCpfToDataBase($dados->cpf);
			//print_r($dados);die();
			// Defina as regras de validação para os campos
			$this->form_validation->set_rules('nome', 'Nome', 'required');
			$this->form_validation->set_rules('sobrenome', 'sobrenome', 'required');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
			$this->form_validation->set_rules('cpf', 'CPF', 'required|exact_length[11]');

			$dados->nome = $dados->nome . " " . $dados->sobrenome;
			unset($dados->sobrenome);

			if ($this->form_validation->run() == FALSE) {
				// Se a validação falhar, crie um array com os erros
				$erros = array(
					'nome' => form_error('nome'),
					'sobrenome' => form_error('sobrenome'),
					'email' => form_error('email'),
					'cpf' => form_error('cpf')
				);
		
				// Converta os erros em JSON
				echo json_encode(array("result"=>"error", "erros"=>$erros, "message"=>"Por favor, preencher todos os campos obrigatórios."));
			} else {
				// Dados válidos, você pode prosseguir com o processamento.
				// Provavelmente, você desejará redirecionar ou realizar alguma outra ação aqui.
				echo json_encode(array("result"=>"success", "message"=>"Cadastro realizado com sucesso!"));
			}
		} catch (TypeError $error){
			header('Location:'.base_url(''));
			exit();
		}
	}

}
