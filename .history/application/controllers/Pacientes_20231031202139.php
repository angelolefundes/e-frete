<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pacientes extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->model('pacientes_model');
		$this->load->library('form_validation');
	}

    //Main de paciente listando todos os pacientes
	public function index()
	{
		$this->load->view('header');
		$this->load->view('pacientes/main');
		$this->load->view('footer');
	}

    //view de cadastro de pacientes
	public function cadastro()
	{
		$this->load->view('header');
		$this->load->view('pacientes/form_cadastro');
		$this->load->view('footer');
	}

	//listar data table
    public function listToDataTable(){
            try{
                    $draw = intval($this->input->post("draw"));
                    $start = intval($this->input->post("start"));
                    $length = intval($this->input->post("length"));
                    $search = $this->input->post("search");

                    $search = $search['value'];

                    $employees = $this->pacientes_model->listAll($length, $start, $search);
                    $data = array();

                    foreach($employees as $values => $rows)
                    {
                        
                        $data[]= array(
                            $rows->nome,
                            $rows->email,
                            $rows->cpf,
                            '<center>
                                <button class="btn btn-primary editar" title="Editar" alt="Editar" data-id="'.$rows->id.'" onClick="window.open(\''.base_url().'admin/advogados/editar/'.$rows->id.'\',\'_parent\'); return false;" style="border: none !important">
                                    <i style="size: 30px !important" class="fa fa-edit fa-2x"></i>
                                </button>
                                <button class="btn btn-primary ativar_desativar" title="Ativar/Desativar" alt="Ativar/Desativar" data-id="'.$rows->id.'" data-status="" data-nome="'.$rows->nome.'" style="border: none !important">
                                    <i style="size: 30px !important" class="fa fa-eye fa-2x"></i>
                                </button>
                            </center>'
                
                        );
                    }

                    $total_employees = $this->countDataTable();
                    $output = array(
                        "draw" => $draw,
                        "recordsTotal" => $total_employees,
                        "recordsFiltered" => $total_employees,
                        "data" => $data
                    );
                    echo json_encode($output);
                    exit();
            } catch (TypeError $error){
                header('Location:'.base_url(''));
                exit();
            }
    }

    //Contador para o datatable
    private function countDataTable(){
        try{
                $query = $this->db->select("count(*) as num")->get("pacientes");
                $result = $query->row();
                if(isset($result)) return $result->num;
                return 0;
        } catch (TypeError $error){
            header('Location:'.base_url(''));
            exit();
        }
    }

	public function gravarCadastro()
    {
        try{
			$dados = (object)$this->input->post(null, true);
			$dados->cpf = Funcoes_diversas::formatCpfToDataBase($dados->cpf);
			
			// Defina as regras de validação para os campos
			$this->form_validation->set_rules('cns', 'Cns', 'required');
			$this->form_validation->set_rules('nome', 'Nome', 'required');
			$this->form_validation->set_rules('sobrenome', 'sobrenome', 'required');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
			$this->form_validation->set_rules('cpf', 'CPF', 'required');
			
			$dados->nome = $dados->nome . " " . $dados->sobrenome;
			unset($dados->sobrenome);

			if ($this->form_validation->run() == FALSE) {
				// Se a validação falhar, crie um array com os erros
				$erros = array();

                if (!empty(form_error('cns'))) { //se cns for vazio adiciona nome ao array $erros
					$erros['cns'] = form_error('cns');
				}
                if (!empty(form_error('nome'))) { //se nome for vazio adiciona nome ao array $erros
					$erros['nome'] = form_error('nome');
				}
				if (!empty(form_error('sobrenome'))) { //se sobrenome for vazio adiciona nome ao array $erros
					$erros['sobrenome'] = form_error('sobrenome');
				}
				if (!empty(form_error('email'))) { //se email for vazio adiciona nome ao array $erros
					$erros['email'] = form_error('email');
				}
				if (!empty(form_error('cpf'))) { //se cpf for vazio adiciona nome ao array $erros
					$erros['cpf'] = form_error('cpf');
				}
		
				// Converta os erros em JSON
				echo json_encode(array("result"=>"error", "erros"=>$erros, "message"=>"Por favor, preencher todos os campos obrigatórios."));
			} else {
				$result = $this->pacientes_model->insert((object)$dados);
				if($result['lines'] > 0){
                    echo json_encode(array("result"=>"success", "message"=>"Cadastro realizado com sucesso!"));
				}else{
					echo json_encode(array("result"=>"error", "message"=>$result['error']['message']));
				}
			}
		} catch (TypeError $error){
			header('Location:'.base_url(''));
			exit();
		}
	}

}
