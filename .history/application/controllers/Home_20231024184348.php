<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->model('estados_model');
	}

	public function index()
	{
		$this->load->view('header');
		$this->load->view('main');
		$this->load->view('footer');
	}

	public function cadastro()
	{
		$this->load->view('header');
		$this->load->view('cadastro');
		$this->load->view('footer');
	}

	public function suporte()
	{
		$this->load->view('header');
		$this->load->view('suporte');
		$this->load->view('footer');
	}

	public function estados()
	{
		// Recupere os estados do banco de dados
		$estados = $this->estados_model->getAll(); 
	
		// Retorne os estados em formato JSON.
		echo json_encode(array("estados" => $estados));
	}

	public function getCidadesByUf()
	{
		// Obtém o estado_id da solicitação AJAX (enviado a partir do JavaScript)
		$estado_id = $this->input->post('estado_id');

		// Chame a função do modelo para obter as cidades do estado
		$cidades = $this->estados_model->getCidadesByUf($estado_id);

		// Retorne as cidades em formato JSON
		echo json_encode(array("cidades" => $cidades));
	}

	public function gravarCadastro()
    {
        try{
			$dados = (object)$this->input->post(null, true);
			//validar dados
			$this->form_validation->set_rules('nome', 'Nome', 'required', array('required' => 'Por favor, informe o nome completo do usuário.'));
			$this->form_validation->set_rules('login', 'Login', 'required|valid_email', array('required' => 'Por favor, informe um email válido.'));           
			//fim
			if($this->form_validation->run() == FALSE && !isset($dados->ativo)){
				$arrayErros = explode("\n", strip_tags(validation_errors()));
				$dataArray = array();
				foreach($arrayErros as $item):
					$dataArray[]= array($item);
				endforeach;
				array_pop($dataArray);
				echo json_encode(array("result"=>"warning_validate", "erros"=>$dataArray), JSON_UNESCAPED_UNICODE);
			}else{
				
				if($result["lines"] > 0){
					
				}else{
				}
			}
		} catch (TypeError $error){
			header('Location:'.base_url(''));
			exit();
		}
	}

}
