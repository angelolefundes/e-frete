<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->model('estados_model');
	}

	public function index()
	{
		$this->load->view('header');
		$this->load->view('main');
		$this->load->view('footer');
	}

	public function cadastro()
	{
		$this->load->view('header');
		$this->load->view('cadastro');
		$this->load->view('footer');
	}

	public function suporte()
	{
		$this->load->view('header');
		$this->load->view('suporte');
		$this->load->view('footer');
	}

	public function estados()
	{
		// Recupere os estados do banco de dados
		$estados = $this->estados_model->getAll(); 
	
		// Retorne os estados em formato JSON.
		echo json_encode(array("estados" => $estados));
	}

	public function getCidadesByUf()
	{
		// Obtém o estado_id da solicitação AJAX (enviado a partir do JavaScript)
		$estado_id = $this->input->post('estado_id');

		// Chame a função do modelo para obter as cidades do estado
		$cidades = $this->estados_model->getCidadesByUf($estado_id);

		// Retorne as cidades em formato JSON
		echo json_encode(array("cidades" => $cidades));
	}

	public function gravarCadastro()
    {
        try{
			$dados = (object)$this->input->post(null, true);
			$dados->nome = $dados->nome . " " . $dados->sobrenome;
			unset($dados->sobrenome);
			if($dados->cpf == ""){
				echo json_encode(array("result"=>"error", "message"=>"Campo CPF é obrigatório."));
			}else{
				echo json_encode(array("result"=>"success", "message"=>"Cadastro realizado com sucesso.", "nome"=>$dados->nome));
			}
		} catch (TypeError $error){
			header('Location:'.base_url(''));
			exit();
		}
	}

}
