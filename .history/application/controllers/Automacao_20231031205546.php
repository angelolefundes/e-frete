<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Automacao extends CI_Controller {

	public function __construct(){
        parent::__construct();
	}

    //Main de paciente listando todos os pacientes
	public function ligar()
	{
        $acao = $this->input->post("acao");

		Funcoes_diversas::activeLampada($acao);
	}
}
