<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Estados extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->model('estados_model');
	}

    public function getEstados()
	{
		// Recupere os estados do banco de dados
		$estados = $this->estados_model->getAll(); 
	
		// Retorne os estados em formato JSON.
		echo json_encode(array("estados" => $estados));
	}

	public function getCidadesByUf()
	{
		// Obtém o estado_id da solicitação AJAX (enviado a partir do JavaScript)
		$estado_id = $this->input->post('estado_id');

		// Chame a função do modelo para obter as cidades do estado
		$cidades = $this->estados_model->getCidadesByUf($estado_id);

		// Retorne as cidades em formato JSON
		echo json_encode(array("cidades" => $cidades));
	}
}
