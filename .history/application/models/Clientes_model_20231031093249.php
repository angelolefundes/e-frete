<?php

class Clientes_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->table = "clientes";
    }

    /*function listAll($length = null, $start = null, $search = null)
    {
        //Monta a consulta
        $this->db->select();
        $this->db->from();
        $this->db->join();
        if ($search != null):
            $this->db->like(, $search);
            $this->db->or_like(, $search);
        elseif ($length != null || $start != null):
            $this->db->limit($length, $start);
        endif;

        $this->db->order_by(, "ASC");
        //Faz a consulta
        $query = $this->db->get();
        //Retorna todos os registros
        return $query->result();
    }*/

    /*function getById($acao = null){
        //Monta a consulta
        $this->db->select("*");
        $this->db->from();
        $this->db->where("", $acao);
        //Faz a consulta
        $query = $this->db->get();
        //Retorna todos os registros
        return $query->row();
    }*/

    function insert($object)
    {
        //Inicia a transação
        $this->db->trans_start();
        //Insere o objeto no banco
        $this->db->insert($this->table, $object, true);
        //Pega a mensagem de erro
        $result['error'] = $this->db->error();
        //Pega a quantidade de linhas afetada
        $result['lines'] = $this->db->affected_rows();
        //Pega o ultimo id inserido
        //$result['id'] = $this->db->insert_id();
        //Finaliza a transação
        $this->db->trans_complete();
        //Retorna um array com as informações
        return $result;
    }

    /*public function update($object)
    {
        //Inicia a transação
        $this->db->trans_start();
        //Monta a cosulta
        $this->db->set($object, null, true);
        $this->db->where("id", $object->id);
        //Faz o update no banco
        $this->db->update($this->table);
        //Pega a mensagem de erro
        $result = $this->db->error();
        //Pega a quantidade de linhas afetada
        $result['lines'] = $this->db->affected_rows();
        //Finaliza a transação
        $this->db->trans_complete();
        //Retorna um array com as informações
        return $result;
    }*/

    /*function delete() {
        return $this->db->delete();
    }*/    
}
