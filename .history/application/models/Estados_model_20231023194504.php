<?php
class Estados_model extends CI_Model
{
    public function getAll()
    {
        // Execute a consulta SQL para obter todos os estados da tabela 'tb_estados'
        $query = $this->db->get('estados');

        // Retorne os resultados como um array de objetos
        return $query->result();
    }

    public function getCidadesByUf($estado_id)
    {
        // Execute a consulta SQL para obter as cidades do estado especificado
        $this->db->where('estado_id', $estado_id);
        $query = $this->db->get('cidades');

        // Retorne os resultados como um array de objetos
        return $query->result();
    }
}
