<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Funcoes_diversas
{
    //$CI =& get_instance();

    //Método para executar alguma URL via cURL (GET)
    public static function activeLampada($url = null){

        $ligar_url = "http://10.22.0.191/ligar_porta01";
        $ligar_ch = curl_init();
        curl_setopt($ligar_ch, CURLOPT_URL, $ligar_url);
        curl_setopt($ligar_ch, CURLOPT_RETURNTRANSFER, 1);
        curl_exec($ligar_ch);
        curl_close($ligar_ch);

    }   
    
    //consulta cnpj
    public static function selectCnpj($cnpj = null){
        $url = "http://receitaws.com.br/v1/cnpj/".$cnpj;

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);
        return json_decode($resp);
    }

    //Valida se email é válido
    public static function validateEmail($email = null){
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            echo "O endereço de e-mail é válido.";
        } else {
            echo "O endereço de e-mail não é válido.";
        }
    }    
    //Método para formatar moeda de formato BR para formato decimal(10,2)
    public static function formatMoneyInBrToDecimal($valor = null) {
        $verificaPonto = ".";
        if(strpos("[".$valor."]", "$verificaPonto")):
            $valor = str_replace('.','', $valor);
            $valor = str_replace(',','.', $valor);
        else:
            $valor = str_replace(',','.', $valor);
        endif;

        return $valor;
    }

    //converter data/hora de Unix/Epoch para DateTime no formato yyyy-mm-dd USA
    //exemplo: 2001-01-01
    public static function formatDateInUnixToDateTimeUsa($data)
    {
        $date = date_create();
        date_timestamp_set($date, $data);
        return date_format($date, 'Y-m-d');
    }

    //converter data/hora de Unix/Epoch para DateTime no formato dd/mm/yyyy BR
    //exemplo: 01/01/2001
    public static function formatDateInUnixToDateTimeBr($data)
    {
        $date = date_create();
        date_timestamp_set($date, $data);
        return date_format($date, 'd/m/Y');
    }

    //converter data BR para Data no formato yyyy-mm-dd USA
    //exemplo: 2001-01-01
    public static function formatDateBrToDateUsa($data)
    {
        return implode("-", array_reverse(explode("/", $data)));
    }

    //converter data/hora de Unix para DateTime no formato dd/mm/yyyy H:i
    //exemplo: 01/01/2001 10:30
    public static function formatDateTimeInUnixToDateTimeBr($data)
    {
        $date = date_create();
        date_timestamp_set($date, $data);
        return date_format($date, 'd/m/Y H:i');
    }

    //converter data/hora de Unix para DateTime no formato yyyy/mm/dd H:i
    //exemplo: 2001-01-01 10:30
    public static function formatDateTimeInUnixToDateTimeUsa($data)
    {
        $date = date_create();
        date_timestamp_set($date, $data);
        return date_format($date, 'Y-m-d H:i');
    }

    //converter data/hora de Unix para formato "16/11/2022T20:30" 
    //utilizado para input type "datetime-local" utilizado em formulários
    public static function formatDateTimeInUnixToDateTimeLocalBr($data)
    {
        $date = date_create();
        date_timestamp_set($date, $data);
        $data_hora_ini = date_format($date, 'd/m/Y H:i');
        $array = explode(" ", $data_hora_ini);
        $data = $array[0];
        $hora = $array[1];
        return $data."T".$hora;
    }

    //formatar datatime-local para Unix/Epoch
    //exemplo: formata 01/02/2022T10:30 para isso 1668655853
    public static function formatDateTimeLocalBrToUnix($data = null){
        $array = explode("T", $data);
        $data = $array[0] . " " . $array[1];
        $data = strtotime($data);
        return $data;
    }

    //formatar data USA para Unix/Epoch
    //exemplo: formata 2023-01-01 para isso 1668655853
    public static function formatDateUsaToUnix($data = null){
        $data = strtotime($data);
        return $data;
    }

    //formatar datatime-local para BR
    //exemplo: formata 2022-02-01T10:30 para isso 01/02/2022 10:30
    public static function formatDateTimeLocalToBr($data = null){
        $array = explode("T", $data);
        $data = implode("/", array_reverse(explode("-", $array[0]))) . " " . $array[1];
        return $data;
    }

    //converte data/Hora BR para UNIX/Epoch
    public static function formatInDateBrToUnix($data)
    {
        $data = strtotime(str_replace('/', '-', $data));
        return $data;
    }    

    //valida se uma data BR é verdadeira
    public static function validateDate($data){
        $data = explode("/","$data"); // fatia a string $dat em pedados, usando / como referência
        $d = $data[0];
        $m = $data[1];
        $y = $data[2];

        // verifica se a data é válida!
        // 1 = true (válida)
        // 0 = false (inválida)
        $res = checkdate($m,$d,$y);
        if ($res == 1){
            return true;
        } else {
            return false;
        }
    }

    //Este método realiza a criptografia e descriptografa dados
    //$action pode ser encrypt ou decrypt
    //exemplo de uso: encrypt_decrypt("encrypt", "123456");
    public static function encrypt_decrypt($action, $string)
    {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'a6a94e132de13792ec3f1477dcbeb296574cea25'; //sha1
        $secret_iv = '1fdd8a43a4fdc777c9b153d2e6561755'; //md5
        // hash
        $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        if ($action == 'encrypt') {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if ($action == 'decrypt') {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        return $output;
    }

    //remover pontos e traço antes de uma string CPF para salvar
    //na base de dados apenas os números
    public static function formatCpfToDataBase($cpf){

        // Elimina possivel mascara
        $cpf = preg_replace("/[^0-9]/", "", $cpf);
        $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);

        return $cpf;
    }

    //remover caracteres especiais de telefone
    //para salvar na base de dados apenas números
    public static function formatPhoneToDataBase($telefone){
        // Elimina possivel mascara
        $telefone = trim($telefone);
        $telefone = str_replace("(", "", $telefone);
        $telefone = str_replace(")", "", $telefone);
        $telefone = str_replace(" ", "", $telefone);
        $telefone = str_replace("-", "", $telefone);
        return $telefone;
    }

    //remove caracters especiais em geral de uma determinada string
    public static function formatStringRemoveCaracterEspecial($string){
        return preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ç|ḉ)/","/(Ç|Ḉ)/","/(ñ)/","/(Ñ)/"),explode(" ","a A e E i I o O u U c C n N"),$string);
    }

    //Método para deletar um determinado arquivo em um diretório
    public static function deleteFile($patch = null)
    {
        $files = glob($patch.'*'); // get all file names
        foreach($files as $file){ // iterate files
            if(is_file($file))
                unlink($file); // delete file
        }
    }
    
    //Calcular qtde de dias no intervalo de data inicio e data atual
    public static function diffDateDays($inicio = null){
        $data_inicial = $inicio;
        $data_final = date("Y") . "-" . date("m") . "-" . date("d");


        $start_date = new DateTime($data_inicial);
        $since_start = $start_date->diff(new DateTime($data_final));
        $total = $since_start->days;
        $anos = $since_start->y;
        $meses = $since_start->m;
        $dias = $since_start->d;
        $horas = $since_start->h;
        $minutos = $since_start->i;
        $segundos = $since_start->s;

        return $dias;
    }

    //Calcular quantidade de anos entre duas datas
    //exemplo de uso: diffDateTime("03/11/2022T05:30:00", null);
    public static function diffDateTimeYear($inicio = null, $fim = null){
        $data_inicial = $inicio;

        if($fim == "") {
            $data_final = date("d") . "/" . date("m") . "/" . date("Y") . "T" . date("H:i");
        }else{
            $data_final =  $fim;
        }

        $arrayDtInicio = explode("T", $data_inicial);
        $data_hora_inicio_flag = implode("-", array_reverse(explode("/", $arrayDtInicio[0])));
        $data_hora_inicio = $data_hora_inicio_flag . " " . $arrayDtInicio[1];
        $data_hora_inicio = str_replace("T", " ", $data_hora_inicio);

        $arrayDtFim = explode("T", $data_final);
        $data_hora_fim_flag = implode("-", array_reverse(explode("/", $arrayDtFim[0])));
        $data_hora_fim = $data_hora_fim_flag . " " . $arrayDtFim[1];
        $data_hora_fim = str_replace("T", " ", $data_hora_fim);

        $start_date = new DateTime($data_hora_inicio);
        $since_start = $start_date->diff(new DateTime($data_hora_fim));
        $total = $since_start->days;
        $anos = $since_start->y;
        $meses = $since_start->m;
        $dias = $since_start->d;
        $horas = $since_start->h;
        $minutos = $since_start->i;
        $segundos = $since_start->s;

        return $anos;
    }
    

    //Calcular quantidade de meses entre duas datas
    //exemplo de uso: diffDateTime("03/11/2022T05:30:00", null);
    public static function diffDateTimeMonth($inicio = null, $fim = null){
        $data_inicial = $inicio;

        if($fim == "") {
            $data_final = date("d") . "/" . date("m") . "/" . date("Y") . "T" . date("H:i");
        }else{
            $data_final =  $fim;
        }

        $arrayDtInicio = explode("T", $data_inicial);
        $data_hora_inicio_flag = implode("-", array_reverse(explode("/", $arrayDtInicio[0])));
        $data_hora_inicio = $data_hora_inicio_flag . " " . $arrayDtInicio[1];
        $data_hora_inicio = str_replace("T", " ", $data_hora_inicio);

        $arrayDtFim = explode("T", $data_final);
        $data_hora_fim_flag = implode("-", array_reverse(explode("/", $arrayDtFim[0])));
        $data_hora_fim = $data_hora_fim_flag . " " . $arrayDtFim[1];
        $data_hora_fim = str_replace("T", " ", $data_hora_fim);

        $start_date = new DateTime($data_hora_inicio);
        $since_start = $start_date->diff(new DateTime($data_hora_fim));
        $total = $since_start->days;
        $anos = $since_start->y;
        $meses = $since_start->m;
        $dias = $since_start->d;
        $horas = $since_start->h;
        $minutos = $since_start->i;
        $segundos = $since_start->s;

        return $meses;
    }

    //Método para calcular diferença em dias, horas e minutos entre uma data/hora e outra
    //exemplo de uso: diffDateTime("03/11/2022T05:30:00", null);
    public static function diffDateTime($inicio = null, $fim = null){
        $data_inicial = $inicio;

        if($fim == "") {
            $data_final = date("d") . "/" . date("m") . "/" . date("Y") . "T" . date("H:i");
        }else{
            $data_final =  $fim;
        }

        $arrayDtInicio = explode("T", $data_inicial);
        $data_hora_inicio_flag = implode("-", array_reverse(explode("/", $arrayDtInicio[0])));
        $data_hora_inicio = $data_hora_inicio_flag . " " . $arrayDtInicio[1];
        $data_hora_inicio = str_replace("T", " ", $data_hora_inicio);

        $arrayDtFim = explode("T", $data_final);
        $data_hora_fim_flag = implode("-", array_reverse(explode("/", $arrayDtFim[0])));
        $data_hora_fim = $data_hora_fim_flag . " " . $arrayDtFim[1];
        $data_hora_fim = str_replace("T", " ", $data_hora_fim);

        $start_date = new DateTime($data_hora_inicio);
        $since_start = $start_date->diff(new DateTime($data_hora_fim));
        $total = $since_start->days;
        $anos = $since_start->y;
        $meses = $since_start->m;
        $dias = $since_start->d;
        $horas = $since_start->h;
        $minutos = $since_start->i;
        $segundos = $since_start->s;

        $html = $dias." dias ".$horas."h ".$minutos."m";

        return $html;
    }

    //Método retorna o cálculo em horas (inteiro) da diferença de uma data-hora
    //até a data-hora atual do servidor
    public static function diffDateTimeToInteger($dataHoraInicial = null)
    {
        $datatime1 = new DateTime($dataHoraInicial);
        $dataAtual = date("Y/m/d H:i:s");

        $datatime2 = new DateTime($dataAtual);

        $data1 = $datatime1->format('Y-m-d H:i:s');
        $data2 = $datatime2->format('Y-m-d H:i:s');

        $diff = $datatime1->diff($datatime2);
        $horas = $diff->h + ($diff->days * 24);
        return $horas;
    }

    //Método para formatar mês de inteiro para string
    public static function formatMonth($mes)
    {
        switch($mes){
            case 1:
                $mes = "Janeiro";
                break;
            case 2:
                $mes = "Fevereiro";
                break;
            case 3:
                $mes = "Março";
                break;
            case 4:
                $mes = "Abril";
                break;
            case 5:
                $mes = "Maio";
                break;
            case 6:
                $mes = "Junho";
                break;
            case 7:
                $mes = "Julho";
                break;
            case 8:
                $mes = "Agosto";
                break;
            case 9:
                $mes = "Setembro";
                break;
            case 10:
                $mes = "Outubro";
                break;
            case 11:
                $mes = "Novembro";
                break;
            case 12:
                $mes = "Dezembro";
                break;
        }
        return $mes;
    }

    //Converter hora para minutos
    public static function inHoursToMinutes($hora)
    {
        $partes = explode(":", $hora);
        $minutos = $partes[0]*60+$partes[1];
        return ($minutos);
    }

   //Método para verificar se um determinado IP está online
    public static function getPing($host, $port, $timeout)
    {       
        $tB = microtime(true); 
        $fP = @fSockOpen($host, $port, $errno, $errstr, $timeout); 
        if (!$fP) { return "down"; } 
        $tA = microtime(true); 
        return round((($tA - $tB) * 1000), 0)." ms";
    }

    //primeiro dia do mês
    public static function firstDayMonth($mes = null, $ano = null){
        $data_incio = mktime(0, 0, 0, $mes , 1 , $ano);
        return date('Y-m-d', $data_incio);
    }

    //último dia do mês
    public static function secondDayMonth($mes = null, $ano = null){
        $data_fim = mktime(23, 59, 59, $mes, date("t"), $ano);
        return date('Y-m-d H:i:s', $data_fim);
    }

    //primeiro dia do mês anterior ao mês atual
    public static function firstDayMonthBefore($inicio = null, $fim = null){
        if($inicio != ""){
            return mktime(0, 0, 0, date('m')-1 , 1 , date('Y'));
        }
        elseif($fim != ""){
            return mktime(23, 59, 59, date('m')+1, date('d')-date('j'), date('Y'));
        }else{
            return "Sem dados.";
        }
    }

    //Método que imprime a data de início da semana e a data final da semana
    //exemplo de uso:
    /*
        Primeiro dia desta semana: Domingo - 02/03/2019
        Primeiro dia da semana passada: domingo - 27/01/2019
        Primeiro dia da próxima semana: domingo - 02/10/2019
        Primeiro dia da semana após a próxima semana:
    */
    public static function getFirstDayOfTheWeek(){
        $firstday = date('d/m/Y', strtotime("sunday -1 week"));
        return $firstday;
        }  
    public static function getFirstDayOfLastWeek(){
        $firstday = date('d/m/Y', strtotime("sunday -2 week"));
        return $firstday;
        }  
    public static function getFirstDayOfNextWeek(){
        $firstday = date('d/m/Y', strtotime("sunday 0 week"));
        return $firstday;
        }  
    public static function getFirstDayOfNextAfterNextWeek(){
        $firstday = date('d/m/Y', strtotime("sunday 1 week"));
        return $firstday;
        } 

    //validar permissões de usuários
    public static function validatePermissionUsers($class = null, $method = null){
        $CI =& get_instance();
        $permissoes = $CI->session->userdata("permissoes");
        $permitido = 0;
        foreach ($permissoes as $k => $v):
            if ($v['class'] == $class AND $v['method'] == $method):
                $permitido += 1;
            endif;
            endforeach;
        return $permitido;
    }

    //carrega página de acesso negado para permissões não permitidas
    public static function accessDenied($class = null, $method= null)
    {
        $CI =& get_instance();
        $dados['class'] = $class;
        $dados['method'] = $method;
        $CI->load->view("admin/header");
        $CI->load->view("admin/top_bar");
        $CI->load->view("admin/menu");
        $CI->load->view("errors/html/error_denied", $dados);
        $CI->load->view("admin/footer");
    }

    //carrega página de acesso negado para permissões não permitidas (Para operação de retorno JSON)
    public static function accessDeniedJSON($class = null, $method= null)
    {
        echo json_encode(array("result"=>"error", "message"=>"Você não tem permissão para esta ação. Contate o administrador de sistemas."));
    }

    //gravar log de ação do usuário
    public static function log($class = null, $method = null, $descricao = null){
        $CI =& get_instance();
        $data = array(
            'descricao' => $descricao,
            'controller' => $class,
            'metodo' => $method,
            'fk_usuario' => $CI->session->userdata('id'),
        );
        
        $CI->db->insert('tb_logs', $data);
    }

    //formatar número de telefone de "+5571992884809" para "+55 71 99288 - 4809"
    public static function formatNumberPhone($number = null){
		$number=substr($number,0,3)." (".substr($number,3,-9).") ".substr($number,5,-4)."-".substr($number,-4);
		// primeiro substr pega apenas o DDD e coloca dentro do (), segundo subtr pega os números do 3º até faltar 4, insere o hifem, e o ultimo pega apenas o 4 ultimos digitos
		return $number;
    }

    //remove acentos da string
    public static function removeAccents($str){
        $a = array("Á", "À", "Â", "Ã", "É", "Ê", "Í", "Ó", "Ô", "Õ", "Ú", "Û", "Ç");
        $b = array("A", "A", "A", "A", "E", "E", "I", "O", "O", "O", "U", "U", "C");
        return str_replace($a, $b, $str);
    }

    //abreviar nomes grandes
    public static function abbreviateName($nome = null) {

        $comAcentos = array('à', 'á', 'â', 'ã', 'ä', 'å', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ù', 'ü', 'ú', 'ÿ', 'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'O', 'Ù', 'Ü', 'Ú');

        $semAcentos = array('a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'y', 'A', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'N', 'O', 'O', 'O', 'O', 'O', '0', 'U', 'U', 'U');

        $nome = explode(" ", $nome); // cria o array $nome com as partes da string
        $num = count($nome); // conta quantas partes o nome tem

        if ($num <= 2) { // se tiver somente nomes, não abrevia
            return $nome[0]." ".$nome[1]; // retorna nome original
        } else { // pelo contrário executa a função
            $count = 0;
            $novo_nome = ''; // variavel que irá concatenar as partes do nome

            foreach ($nome as $var) { // loop no array
                if ($count == 0) {
                    $novo_nome .= $var . ' ';
                } // mostra primeiro nome

                $count++; // acrescenta +1 na no contador

                /* agora só irá abreviar os nomes do meio, com a condição abaixo, porém, se for algum contido no array de preposições mais comuns, não irá abreviar */

                if (($count >= 2) && ($count < $num)) {

                    $array = array('do', 'Do', 'DO', 'da', 'Da', 'DA', 'de', 'De', 'DE', 'dos', 'Dos', 'DOS', 'das', 'Das', 'DAS');

                    if (in_array($var, $array)) {
                        $novo_nome .= $var . ' '; // não abreviou
                    } // fim if array
                    else {

                        $novo_nome .= substr($var, 0, 1) . '. '; // abreviou
                    } // fim else
                } // fim if nomes do meio

                if ($count == $num) {
                    $novo_nome .= $var;
                } // mostra último nome, quando o contador (count) alcançar o número total de valores do array $nome
            } // fim foreach

            return str_replace($comAcentos, $semAcentos, $novo_nome); // retorna novo nome
        } // fim else
    }    

    //completar com zero à esquerda
    public static function completeVarLeftZero($var = null){
        return str_pad($var , 8 , '0' , STR_PAD_LEFT);
    }
    
    //consulta e retorno em json para bancos
    public static function bancos(){
            $bancos = array(
                array('code' => '001', 'name' => 'Banco do Brasil'),
                array('code' => '003', 'name' => 'Banco da Amazônia'),
                array('code' => '004', 'name' => 'Banco do Nordeste'),
                array('code' => '021', 'name' => 'Banestes'),
                array('code' => '025', 'name' => 'Banco Alfa'),
                array('code' => '027', 'name' => 'Besc'),
                array('code' => '029', 'name' => 'Banerj'),
                array('code' => '031', 'name' => 'Banco Beg'),
                array('code' => '033', 'name' => 'Banco Santander Banespa'),
                array('code' => '036', 'name' => 'Banco Bem'),
                array('code' => '037', 'name' => 'Banpará'),
                array('code' => '038', 'name' => 'Banestado'),
                array('code' => '039', 'name' => 'BEP'),
                array('code' => '040', 'name' => 'Banco Cargill'),
                array('code' => '041', 'name' => 'Banrisul'),
                array('code' => '044', 'name' => 'BVA'),
                array('code' => '045', 'name' => 'Banco Opportunity'),
                array('code' => '047', 'name' => 'Banese'),
                array('code' => '062', 'name' => 'Hipercard'),
                array('code' => '063', 'name' => 'Ibibank'),
                array('code' => '065', 'name' => 'Lemon Bank'),
                array('code' => '066', 'name' => 'Banco Morgan Stanley Dean Witter'),
                array('code' => '069', 'name' => 'BPN Brasil'),
                array('code' => '070', 'name' => 'Banco de Brasília – BRB'),
                array('code' => '072', 'name' => 'Banco Rural'),
                array('code' => '073', 'name' => 'Banco Popular'),
                array('code' => '074', 'name' => 'Banco J. Safra'),
                array('code' => '075', 'name' => 'Banco CR2'),
                array('code' => '076', 'name' => 'Banco KDB'),
                array('code' => '096', 'name' => 'Banco BMF'),
                array('code' => '104', 'name' => 'Caixa Econômica Federal'),
                array('code' => '107', 'name' => 'Banco BBM'),
                array('code' => '116', 'name' => 'Banco Único'),
                array('code' => '151', 'name' => 'Nossa Caixa'),
                array('code' => '175', 'name' => 'Banco Finasa'),
                array('code' => '184', 'name' => 'Banco Itaú BBA'),
                array('code' => '204', 'name' => 'American Express Bank'),
                array('code' => '208', 'name' => 'Banco Pactual'),
                array('code' => '212', 'name' => 'Banco Matone'),
                array('code' => '213', 'name' => 'Banco Arbi'),
                array('code' => '214', 'name' => 'Banco Dibens'),
                array('code' => '217', 'name' => 'Banco Joh Deere'),
                array('code' => '218', 'name' => 'Banco Bonsucesso'),
                array('code' => '222', 'name' => 'Banco Calyon Brasil'),
                array('code' => '224', 'name' => 'Banco Fibra'),
                array('code' => '225', 'name' => 'Banco Brascan'),
                array('code' => '229', 'name' => 'Banco Cruzeiro'),
                array('code' => '230', 'name' => 'Unicard'),
                array('code' => '233', 'name' => 'Banco GE Capital'),
                array('code' => '237', 'name' => 'Bradesco'),
                array('code' => '241', 'name' => 'Banco Clássico'),
                array('code' => '243', 'name' => 'Banco Stock Máxima'),
                array('code' => '246', 'name' => 'Banco ABC Brasil'),
                array('code' => '248', 'name' => 'Banco Boavista Interatlântico'),
                array('code' => '249', 'name' => 'Investcred Unibanco'),
                array('code' => '250', 'name' => 'Banco Schahin'),
                array('code' => '252', 'name' => 'Fininvest'),
                array('code' => '254', 'name' => 'Paraná Banco'),
                array('code' => '263', 'name' => 'Banco Cacique'),
                array('code' => '265', 'name' => 'Banco Fator'),
                array('code' => '266', 'name' => 'Banco Cédula'),
                array('code' => '300', 'name' => 'Banco de la Nación Argentina'),
                array('code' => '318', 'name' => 'Banco BMG'),
                array('code' => '320', 'name' => 'Banco Industrial e Comercial'),
                array('code' => '356', 'name' => 'ABN Amro Real'),
                array('code' => '341', 'name' => 'Itau'),
                array('code' => '347', 'name' => 'Sudameris'),
                array('code' => '351', 'name' => 'Banco Santander'),
                array('code' => '353', 'name' => 'Banco Santander Brasil'),
                array('code' => '366', 'name' => 'Banco Societe Generale Brasil'),
                array('code' => '370', 'name' => 'Banco WestLB'),
                array('code' => '376', 'name' => 'JP Morgan'),
                array('code' => '389', 'name' => 'Banco Mercantil do Brasil'),
                array('code' => '394', 'name' => 'Banco Mercantil de Crédito'),
                array('code' => '399', 'name' => 'HSBC'),
                array('code' => '409', 'name' => 'Unibanco'),
                array('code' => '412', 'name' => 'Banco Capital'),
                array('code' => '422', 'name' => 'Banco Safra'),
                array('code' => '453', 'name' => 'Banco Rural'),
                array('code' => '456', 'name' => 'Banco Tokyo Mitsubishi UFJ'),
                array('code' => '464', 'name' => 'Banco Sumitomo Mitsui Brasileiro'),
                array('code' => '477', 'name' => 'Citibank'),
                array('code' => '479', 'name' => 'Itaubank (antigo Bank Boston)'),
                array('code' => '487', 'name' => 'Deutsche Bank'),
                array('code' => '488', 'name' => 'Banco Morgan Guaranty'),
                array('code' => '492', 'name' => 'Banco NMB Postbank'),
                array('code' => '494', 'name' => 'Banco la República Oriental del Uruguay'),
                array('code' => '495', 'name' => 'Banco La Provincia de Buenos Aires'),
                array('code' => '505', 'name' => 'Banco Credit Suisse'),
                array('code' => '600', 'name' => 'Banco Luso Brasileiro'),
                array('code' => '604', 'name' => 'Banco Industrial'),
                array('code' => '610', 'name' => 'Banco VR'),
                array('code' => '611', 'name' => 'Banco Paulista'),
                array('code' => '612', 'name' => 'Banco Guanabara'),
                array('code' => '613', 'name' => 'Banco Pecunia'),
                array('code' => '623', 'name' => 'Banco Panamericano'),
                array('code' => '626', 'name' => 'Banco Ficsa'),
                array('code' => '630', 'name' => 'Banco Intercap'),
                array('code' => '633', 'name' => 'Banco Rendimento'),
                array('code' => '634', 'name' => 'Banco Triângulo'),
                array('code' => '637', 'name' => 'Banco Sofisa'),
                array('code' => '638', 'name' => 'Banco Prosper'),
                array('code' => '643', 'name' => 'Banco Pine'),
                array('code' => '652', 'name' => 'Itaú Holding Financeira'),
                array('code' => '653', 'name' => 'Banco Indusval'),
                array('code' => '654', 'name' => 'Banco A.J. Renner'),
                array('code' => '655', 'name' => 'Banco Votorantim'),
                array('code' => '707', 'name' => 'Banco Daycoval'),
                array('code' => '719', 'name' => 'Banif'),
                array('code' => '721', 'name' => 'Banco Credibel'),
                array('code' => '734', 'name' => 'Banco Gerdau'),
                array('code' => '735', 'name' => 'Banco Pottencial'),
                array('code' => '738', 'name' => 'Banco Morada'),
                array('code' => '739', 'name' => 'Banco Galvão de Negócios'),
                array('code' => '740', 'name' => 'Banco Barclays'),
                array('code' => '741', 'name' => 'BRP'),
                array('code' => '743', 'name' => 'Banco Semear'),
                array('code' => '745', 'name' => 'Banco Citibank'),
                array('code' => '746', 'name' => 'Banco Modal'),
                array('code' => '747', 'name' => 'Banco Rabobank International'),
                array('code' => '748', 'name' => 'Banco Cooperativo Sicredi'),
                array('code' => '749', 'name' => 'Banco Simples'),
                array('code' => '751', 'name' => 'Dresdner Bank'),
                array('code' => '752', 'name' => 'BNP Paribas'),
                array('code' => '753', 'name' => 'Banco Comercial Uruguai'),
                array('code' => '755', 'name' => 'Banco Merrill Lynch'),
                array('code' => '756', 'name' => 'Banco Cooperativo do Brasil'),
                array('code' => '757', 'name' => 'KEB'),
            );
    
            return $bancos;
    }

    //usando para function que monta balancete financeiro
    public static function validaHierarquiaNatFinanceira($filho, $pai){
        if($filho[0] == $pai[0]){
            return true;
        }else{
            return false;
        }
    }
}
