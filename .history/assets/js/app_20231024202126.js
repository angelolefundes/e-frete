$(document).ready(function() {
    // Limpe o select e adicione uma opção padrão.
    $('#uf').empty();
    $('#uf').append($('<option>', {
        value: '',
        text: 'Carregando...'
    }));

    // Faça uma solicitação AJAX para obter os estados do seu endpoint no CodeIgniter.
    $.ajax({
        url: base_url + 'home/estados/', // Substitua pela URL correta do seu endpoint
        type: 'GET',
        dataType: 'json',
        success: function(data) {
            // Limpe o select e adicione uma opção padrão.
            $('#uf').empty();
            $('#uf').append($('<option>', {
                value: '',
                text: 'Escolha uma opção'
            }));

            // Preencha o select com os estados retornados.
            $.each(data.estados, function(key, value) {
                $('#uf').append($('<option>', {
                    value: value.id, 
                    text: value.nome 
                }));
            });
        }
    });

    // Capturando quando ocorrer escolha de UF popular o <select> de cidades
    $('#uf').on('change', function() {
            var estado_id = $(this).val(); // Obtém o ID do estado selecionado
            $('#cidades').empty();
            $('#cidades').append($('<option>', {
                value: '',
                text: 'Carregando...'
            }));

            // Faça uma solicitação AJAX para obter as cidades do estado selecionado
            $.ajax({
                url: base_url + 'home/getCidadesByUf', // endpoint
                type: 'POST',
                dataType: 'json',
                data: {
                    estado_id: estado_id
                },
                success: function(data) {
                    // Limpe o select e adicione uma opção padrão
                    $('#cidades').empty();
                    $('#cidades').append($('<option>', {
                        value: '',
                        text: 'Escolha uma opção'
                    }));
    
                    // Preencha o select com as cidades retornadas
                    $.each(data.cidades, function(key, value) {
                        $('#cidades').append($('<option>', {
                            value: value.id, 
                            text: value.nome 
                        }));
                    });
                }
            });
    });

    // Envio de dados do cadastro de usuários
    $("#j-forms-cadastro").on("click", "button#btn-gravar-cadastro", function () {
        $("#btn-gravar-cadastro").attr("disabled", true);
        var dados = new FormData();
        
        //Form data
        var form_data = $('#j-forms-cadastro').serializeArray();
        $.each(form_data, function (key, input) {
            dados.append(input.name, input.value);
        });
        $.ajax({
            method: 'POST',
            url: base_url + "home/gravarCadastro",
            processData: false,
            contentType: false,
            data: dados,
            dataType: 'JSON',
            success: function (data) {
                if(data.result == "error"){
                    toastr[data.result](data.message, "Erro!");
                }else{
                    toastr[data.result](data.message, "Mensagem");
                }
            },
            error: function (request, status, error) {
                toastr["error"]("Ocorreu um erro na requisição solicitada.", "Erro!");
                $("#btn-gravar-cadastro").attr("disabled", false);
            }
        });
    });
});
