$(document).ready(function() {
    // Limpe o select e adicione uma opção padrão.
    $('#uf').empty();
    $('#uf').append($('<option>', {
        value: '',
        text: 'Carregando...'
    }));

    // Faça uma solicitação AJAX para obter os estados do seu endpoint no CodeIgniter.
    $.ajax({
        url: base_url + 'home/estados/', // Substitua pela URL correta do seu endpoint
        type: 'GET',
        dataType: 'json',
        success: function(data) {
            // Limpe o select e adicione uma opção padrão.
            $('#uf').empty();
            $('#uf').append($('<option>', {
                value: '',
                text: 'Escolha uma opção'
            }));

            // Preencha o select com os estados retornados.
            $.each(data.estados, function(key, value) {
                $('#uf').append($('<option>', {
                    value: value.id, 
                    text: value.nome 
                }));
            });
        }
    });

    // Capturando quando ocorrer escolha de UF popular o <select> de cidades
    $('#uf').on('change', function() {
            var estado_id = $(this).val(); // Obtém o ID do estado selecionado
            var nome = "Maria";
            $('#cidades').empty();
            $('#cidades').append($('<option>', {
                value: '',
                text: 'Carregando...'
            }));

            // Faça uma solicitação AJAX para obter as cidades do estado selecionado
            $.ajax({
                url: base_url + 'home/getCidadesUf', // endpoint
                type: 'POST',
                dataType: 'json',
                data: {
                    estado_id: estado_id,
                    nome_usuario: nome
                },
                success: function(data) {
                    // Limpe o select e adicione uma opção padrão
                    $('#cidades').empty();
                    $('#cidades').append($('<option>', {
                        value: '',
                        text: 'Escolha uma opção'
                    }));
    
                    // Preencha o select com as cidades retornadas
                    $.each(data.cidades, function(key, value) {
                        $('#cidades').append($('<option>', {
                            value: value.id, 
                            text: value.nome 
                        }));
                    });
                }
            });
    });

    // Envio de dados do cadastro de usuários
    $("#j-forms-cadastro").on("click", "button#btn-gravar-cadastro", function () {
        
        
    });
});
