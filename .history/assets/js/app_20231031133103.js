$(document).ready(function() {
    // Limpe o select e adicione uma opção padrão.
    $('#uf').empty();
    $('#uf').append($('<option>', {
        value: '',
        text: 'Carregando...'
    }));
    $(".invalid-feedback").attr("style", "color: red");
                    
    // Faça uma solicitação AJAX para obter os estados do seu endpoint no CodeIgniter.
    $.ajax({
        url: base_url + 'home/estados/', // Substitua pela URL correta do seu endpoint
        type: 'GET',
        dataType: 'json',
        success: function(data) {
            // Limpe o select e adicione uma opção padrão.
            $('#uf').empty();
            $('#uf').append($('<option>', {
                value: '',
                text: 'Escolha uma opção'
            }));

            // Preencha o select com os estados retornados.
            $.each(data.estados, function(key, value) {
                $('#uf').append($('<option>', {
                    value: value.id, 
                    text: value.nome 
                }));
            });
        }
    });

    // Capturando quando ocorrer escolha de UF popular o <select> de cidades
    $('#uf').on('change', function() {
            var estado_id = $(this).val(); // Obtém o ID do estado selecionado
            $('#cidades').empty();
            $('#cidades').append($('<option>', {
                value: '',
                text: 'Carregando...'
            }));

            // Faça uma solicitação AJAX para obter as cidades do estado selecionado
            $.ajax({
                url: base_url + 'home/getCidadesByUf', // endpoint
                type: 'POST',
                dataType: 'json',
                data: {
                    estado_id: estado_id
                },
                success: function(data) {
                    // Limpe o select e adicione uma opção padrão
                    $('#cidades').empty();
                    $('#cidades').append($('<option>', {
                        value: '',
                        text: 'Escolha uma opção'
                    }));
    
                    // Preencha o select com as cidades retornadas
                    $.each(data.cidades, function(key, value) {
                        $('#cidades').append($('<option>', {
                            value: value.id, 
                            text: value.nome 
                        }));
                    });
                }
            });
    });

    $('#cpf').on('focus', function() {
        $(this).mask('000.000.000-00', {reverse: true});
    });

    // Envio de dados do cadastro de usuários
    $("#j-forms-cadastro").on("click", "button#btn-gravar-cadastro", function () {
        $("#btn-gravar-cadastro").attr("disabled", true);
        var dados = new FormData();
        
        //Form data
        var form_data = $('#j-forms-cadastro').serializeArray();
        $.each(form_data, function (key, input) {
            dados.append(input.name, input.value);
        });
        $.ajax({
            method: 'POST',
            url: base_url + "home/gravarCadastro",
            processData: false,
            contentType: false,
            data: dados,
            dataType: 'JSON',
            success: function (data) {
                console.log(data.erros);
                if(data.result == "error"){
                    $("#btn-gravar-cadastro").attr("disabled", false);
                }else{
                    toastr[data.result](data.message, "Parabéns!");   

                }
            },
            error: function (request, status, error) {
                toastr["error"]("Ocorreu um erro na requisição solicitada.", "Erro!");
                $("#btn-gravar-cadastro").attr("disabled", false);
            }
        });
    });

    //Monta o dataTable da lista geral de advogados
    var table = $('#datatable').DataTable({
        "oLanguage": {
            "sProcessing": "<img src='"+base_url+"assets/admin/images/spinner.gif' width='50' height='50'/>",
            "sLengthMenu": "Mostrar _MENU_ registros por pagina",
            "sZeroRecords": "Sem dados para exibir",
            "sInfoEmtpy": "Exibindo 0 a 0 de 0 registros",
            "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
            "sInfoFiltered": "",
            "sSearch": "Procurar",
            "oPaginate": {
                "sFirst":    "Primeiro",
                "sPrevious": "Anterior",
                "sNext":     "Próximo",
                "sLast":     "Último"
            }
        },
        "processing": true,
        "pageLength" : 10,
        "serverSide": true,
        "ordering": false,
        "columnDefs": [ {
            "targets": 'no-sort',
            "orderable": false,
        } ],
        "order": [
            [0, "asc" ]
        ],
        "ajax": {
            url :  base_url+'clientes/listToDataTable',
            type : 'POST'
        },
    });    
});
