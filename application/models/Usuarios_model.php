<?php

class Usuarios_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->table = "mdl_user";
    }
    
    function listAll($length = null, $start = null, $search = null)
    {
        $this->db->select("CONCAT(UPPER(LEFT(firstname, 1)), LOWER(SUBSTRING(firstname FROM 2)), ' ', CASE WHEN lastname = '.' THEN '' ELSE CONCAT(UPPER(LEFT(lastname, 1)), LOWER(SUBSTRING(lastname FROM 2))) END) AS full_name, email", FALSE);
        $this->db->from($this->table);
        $this->db->where('email IS NOT NULL AND email <> "" AND email LIKE "%@%"');
        $this->db->where('deleted', 0);
        $this->db->where('suspended', 0);
    
        if ($search != null) {
            $this->db->group_start();
            $this->db->like("firstname", $search);
            $this->db->or_like("lastname", $search);
            $this->db->or_like("email", $search);
            $this->db->group_end();
        } elseif ($length != null || $start != null) {
            $this->db->limit($length, $start);
        }
    
        $this->db->group_by('email');
        $this->db->having('COUNT(email)', 1);
        
        $this->db->order_by('lastname', 'ASC');
    
        $query = $this->db->get();
    
        return $query->result();
    }
    

    /*function getById($acao = null){
        //Monta a consulta
        $this->db->select("*");
        $this->db->from();
        $this->db->where("", $acao);
        //Faz a consulta
        $query = $this->db->get();
        //Retorna todos os registros
        return $query->row();
    }*/

    function insert($object)
    {
        //Inicia a transação
        $this->db->trans_start();
        //Insere o objeto no banco
        $this->db->insert($this->table, $object, true);
        //Pega a mensagem de erro
        $result = $this->db->error();
        //Pega a quantidade de linhas afetada
        $result["lines"] = $this->db->affected_rows();
        //Pega o ultimo id inserido
        //$result['id'] = $this->db->insert_id();
        //Finaliza a transação
        $this->db->trans_complete();
        //Retorna um array com as informações
        return $result;
    }

    /*public function update($object)
    {
        //Inicia a transação
        $this->db->trans_start();
        //Monta a cosulta
        $this->db->set($object, null, true);
        $this->db->where("id", $object->id);
        //Faz o update no banco
        $this->db->update($this->table);
        //Pega a mensagem de erro
        $result = $this->db->error();
        //Pega a quantidade de linhas afetada
        $result['lines'] = $this->db->affected_rows();
        //Finaliza a transação
        $this->db->trans_complete();
        //Retorna um array com as informações
        return $result;
    }*/

    /*function delete() {
        return $this->db->delete();
    }*/    
}
